using Microsoft.EntityFrameworkCore;
using MoviesGraph.Models;

namespace MoviesGraph.Data
{
    public class SiteDbContext : DbContext
    {
        public SiteDbContext(DbContextOptions<SiteDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Director>().HasData(
                new Director()
                {
                    Id = 1,
                    Name = "Nolan"
                },
                new Director()
                {
                    Id = 2,
                    Name = "Tarantino"
                },
                new Director() 
                {
                    Id = 3,
                    Name = "Coppolla"
                }
            );

            modelBuilder.Entity<Movie>().HasData(
                new 
                {
                    Id = 1,
                    Name = "Pulp Fiction",
                    Year = 1996,
                    DirectorId = 2
                },
                new 
                {
                    Id = 2,
                    Name = "The Dark Knight",
                    Year = 2015,
                    DirectorId = 1
                },
                new 
                {
                    Id = 3,
                    Name = "The Godfather",
                    Year = 1940,
                    DirectorId = 3
                }
            );
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Director> Directors { get; set; }
    }
}