namespace MoviesGraph.GraphQL.MutationHandler
{
    public record AddMovieInput(
        string Name,
        int Year,
        int DirectorId
    );
}