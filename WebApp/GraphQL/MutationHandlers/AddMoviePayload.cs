using MoviesGraph.Models;

namespace MoviesGraph.GraphQL.MutationHandler
{
    public class AddMoviePayload
    {
        public AddMoviePayload(Movie movie)
        {
            Movie = movie;
        }

        public Movie Movie { get; }
    }
}