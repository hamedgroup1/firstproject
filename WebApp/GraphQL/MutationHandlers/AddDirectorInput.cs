namespace MoviesGraph.GraphQL.MutationHandler
{
    public record AddDirectorInput(
        string Name
    );
}