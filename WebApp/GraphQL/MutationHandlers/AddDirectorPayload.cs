using MoviesGraph.Models;

namespace MoviesGraph.GraphQL.MutationHandler
{
    public class AddDirectorPayload
    {
        public AddDirectorPayload(Director director)
        {
            Director = director;
        }

        public Director Director { get; set; }
    }
}