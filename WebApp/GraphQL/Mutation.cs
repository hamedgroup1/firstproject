using System.Threading.Tasks;
using HotChocolate;
using Microsoft.EntityFrameworkCore;
using MoviesGraph.Data;
using MoviesGraph.Extensions;
using MoviesGraph.GraphQL.MutationHandler;
using MoviesGraph.Models;

namespace MoviesGraph.GraphQL
{
    public class Mutation
    {
        [UseApplicationDbContext]
        public async Task<AddMoviePayload> AddMovie(AddMovieInput input, [ScopedService]SiteDbContext context)
        {
            Movie movie = new() 
            {
                Name = input.Name,
                Year = input.Year,
                Director = await context.Directors.FirstOrDefaultAsync(d => d.Id == input.DirectorId)
            };

            await context.Movies.AddAsync(movie);
            await context.SaveChangesAsync();

            return new AddMoviePayload(movie);
        }

        [UseApplicationDbContext]
        public async Task<AddDirectorPayload> AddDirector(AddDirectorInput input, [ScopedService]SiteDbContext context)
        {
            Director director = new()
            {
                Name = input.Name
            };

            await context.Directors.AddAsync(director);
            await context.SaveChangesAsync();

            return new AddDirectorPayload(director);
        }
        
        [UseApplicationDbContext]
        public async Task<AddDirectorPayload> UpdateDirector(int id, AddDirectorInput input, [ScopedService]SiteDbContext context)
        {
            
            Director director = await context.Directors.FirstAsync<Director>(d => d.Id == id);

            director.Name = input.Name;

            context.Directors.Update(director);
            await context.SaveChangesAsync();

            return new AddDirectorPayload(director);
        }

        [UseApplicationDbContext]
        public async Task<AddMoviePayload> UpdateMovie(int id, string name, int? year, int? directorId, [ScopedService]SiteDbContext context)
        {
            Movie oldMovie = await context.Movies.AsNoTracking().SingleOrDefaultAsync(m => m.Id == id);
            Director director = await context.Directors.AsNoTracking().SingleOrDefaultAsync(d => d.Id == directorId) ?? oldMovie.Director;

            Movie newMovie = new()
            {
                Id = id,
                Name = name ?? oldMovie.Name,
                Year = year ?? oldMovie.Year,
                Director = director
            };

            context.Movies.Update(newMovie);
            await context.SaveChangesAsync();

            return new AddMoviePayload(newMovie);
        }
    }
}