using System.Collections.Generic;
using System.Threading.Tasks;
using HotChocolate;
using Microsoft.EntityFrameworkCore;
using MoviesGraph.Data;
using MoviesGraph.Extensions;
using MoviesGraph.Models;

namespace MoviesGraph.GraphQL
{
    public class Query
    {
        [UseApplicationDbContext]
        public async Task<List<Movie>> GetMovies([ScopedService]SiteDbContext context)
        {
            return await context.Movies.Include(m => m.Director).ToListAsync();
        }

        [UseApplicationDbContext]
        public async Task<List<Director>> GetDirectors([ScopedService]SiteDbContext context)
        {
            return await context.Directors.ToListAsync();
        }
    }
}